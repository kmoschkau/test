# Taken from https://www.joseespitia.com/2017/09/15/set-wallpaper-powershell-function/

$url = "https://cinamigos.com/wp-content/uploads/Mandy-Movie-Review-1920x1080.jpg"
Invoke-WebRequest $url -OutFile C:\temp\test.jpg
$Image = "C:\temp\test.jpg"

Add-Type -TypeDefinition @" 
using System; 
using System.Runtime.InteropServices;
 
public class Params
{ 
    [DllImport("User32.dll",CharSet=CharSet.Unicode)] 
    public static extern int SystemParametersInfo (Int32 uAction, 
                                                   Int32 uParam, 
                                                   String lpvParam, 
                                                   Int32 fuWinIni);
}
"@ 
 
$SPI_SETDESKWALLPAPER = 0x0014
$UpdateIniFile = 0x01
$SendChangeEvent = 0x02
 
$fWinIni = $UpdateIniFile -bor $SendChangeEvent
 
$ret = [Params]::SystemParametersInfo($SPI_SETDESKWALLPAPER, 0, $Image, $fWinIni)